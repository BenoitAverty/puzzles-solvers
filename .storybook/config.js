import { configure, addDecorator, addParameters } from "@storybook/react"
import { withKnobs } from "@storybook/addon-knobs"

addDecorator(withKnobs)
addParameters({
  options: {
    hierarchySeparator: /\//,
    storySort: (a, b) => a[1].id.localeCompare(b[1].id)
  },
})

// automatically import all files ending in *.stories.js
configure(require.context("../src", true, /\.stories\.js$/), module)

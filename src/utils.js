export function noop() {}

const noFillValue = Symbol("noFillValue")

/**
 * Returns an array of `n` consecutive integers beginning at `start`.
 *
 * If fillValue is given, instead returns an array of `n` times the fillValue.
 *
 * @param start first integer of the array if fillValue is not given.
 * @param n length of the result array
 * @param fillValue optional value of all the elements of the array. If given, start is ignored.
 *
 * @returns {Array} an array of n elements corresponding to the given parameters.
 */
export function range(start, n, fillValue = noFillValue) {
  return [...Array(n)].map((_, i) =>
    fillValue === noFillValue ? i + start : fillValue,
  )
}

/**
 * Builds a two-dimensional array of the given size representing a grid. The content
 * of each item is the result of the provided callback called with the coordinates of the item.
 *
 * In other words : `mapGridCoordinates(5, 5, myFunction)[i][j] === myFunction(i,j)`
 *
 * @param rows number of rows in the grid.
 * @param columns number of columns in the grid.
 * @param callback function called to populate the arrays.
 * @returns {*[][]}
 */
export function mapGridCoordinates(rows, columns, callback) {
  return range(0, rows).map(i => range(0, columns).map(j => callback(i, j)))
}

/**
 * Toggle the given value in an array.
 *
 * If the value is present, it is removed, otherwise it is added.
 *
 * @param array the initial array
 * @param value the value to add or remove
 * @returns {*[]} a new array with the given value added or removed
 */
export function toggleInArray(array, value) {
  if (!array) {
    return [value]
  }
  return array.includes(value)
    ? array.filter(e => e !== value)
    : array.concat(value).sort()
}

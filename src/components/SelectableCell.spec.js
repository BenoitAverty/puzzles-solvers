import React from "react"
import { render, fireEvent } from "@testing-library/react"

import SelectableCell from "./SelectableCell"

describe("SelectableCell", () => {
  it("Selects when clicked on", () => {
    const onSelectSpy = jest.fn()
    const onAddToSelectionSpy = jest.fn()
    const { getByTestId } = render(
      <SelectableCell
        onSelect={onSelectSpy}
        onAddToSelection={onAddToSelectionSpy}
      >
        <div data-testid="cell-content" />
      </SelectableCell>,
    )

    fireEvent.mouseDown(getByTestId("cell-content"))

    expect(onSelectSpy).toHaveBeenCalledTimes(1)
    expect(onAddToSelectionSpy).not.toHaveBeenCalled()
  })

  it("Adds to selection when mouse enters while button 1 is down", () => {
    const onSelectSpy = jest.fn()
    const onAddToSelectionSpy = jest.fn()
    const { getByTestId } = render(
      <SelectableCell
        onSelect={onSelectSpy}
        onAddToSelection={onAddToSelectionSpy}
      >
        <div data-testid="cell-content" />
      </SelectableCell>,
    )

    fireEvent.mouseEnter(getByTestId("cell-content"), { buttons: 1 })

    expect(onAddToSelectionSpy).toHaveBeenCalledTimes(1)
    expect(onSelectSpy).not.toHaveBeenCalled()
  })

  it("Does not add to selection when mouse button is up", () => {
    const onSelectSpy = jest.fn()
    const onAddToSelectionSpy = jest.fn()
    const { getByTestId } = render(
      <SelectableCell
        onSelect={onSelectSpy}
        onAddToSelection={onAddToSelectionSpy}
      >
        <div data-testid="cell-content" />
      </SelectableCell>,
    )

    fireEvent.mouseEnter(getByTestId("cell-content"))

    expect(onAddToSelectionSpy).not.toHaveBeenCalled()
    expect(onSelectSpy).not.toHaveBeenCalled()
  })
})

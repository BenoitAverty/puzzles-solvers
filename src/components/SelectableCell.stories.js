import React from "react"
import SelectableCell from "./SelectableCell"
import { action } from "@storybook/addon-actions"

export default {
  title: "3. Low level components/SelectableCell",
  component: SelectableCell,
}

const logSelection = action("Cell selected")
const logAddSelection = action("Cell added to selection")
export const Multiple = () => {
  const [selected, setSelectedState] = React.useState({
    a: false,
    b: false,
    c: false,
    d: false,
  })

  return (
    // The div has a size that makes the cell behave like it was in a grid
    <div
      style={{
        width: "100px",
        height: "100px",
        border: "1px solid",
        display: "grid",
        gridTemplateColumns: "50px 50px",
      }}
    >
      <SelectableCell
        selected={selected.a}
        onSelect={() => {
          logSelection(1)
          setSelectedState(() => ({ a: true }))
        }}
        onAddToSelection={() => {
          logAddSelection(1)
          setSelectedState(curr => ({ ...curr, a: true }))
        }}
      >
        1
      </SelectableCell>
      <SelectableCell
        selected={selected.b}
        onSelect={() => {
          logSelection(2)
          setSelectedState(() => ({ b: true }))
        }}
        onAddToSelection={() => {
          logAddSelection(2)
          setSelectedState(curr => ({ ...curr, b: true }))
        }}
      >
        2
      </SelectableCell>
      <SelectableCell
        selected={selected.c}
        onSelect={() => {
          logSelection(3)
          setSelectedState(() => ({ c: true }))
        }}
        onAddToSelection={() => {
          logAddSelection(3)
          setSelectedState(curr => ({ ...curr, c: true }))
        }}
      >
        3
      </SelectableCell>
      <SelectableCell
        selected={selected.d}
        onSelect={() => {
          logSelection(4)
          setSelectedState(() => ({ d: true }))
        }}
        onAddToSelection={() => {
          logAddSelection(4)
          setSelectedState(curr => ({ ...curr, d: true }))
        }}
      >
        4
      </SelectableCell>
    </div>
  )
}

import React from "react"
import PropTypes from "prop-types"
import styled from "@emotion/styled"

const propTypes = {
  children: PropTypes.node,
  /** Left border style */
  left: PropTypes.oneOf(["none", "normal", "bold"]),
  /** Right border style */
  right: PropTypes.oneOf(["none", "normal", "bold"]),
  /** Top border style */
  top: PropTypes.oneOf(["none", "normal", "bold"]),
  /** Bottom border style */
  bottom: PropTypes.oneOf(["none", "normal", "bold"]),
}

/**
 * Manage cell borders.
 *
 * There are 3 styles of borders: none, normal or bold. Each side of the cell can be a different style.
 */
export default function CellBorders({
  left = "normal",
  right = "normal",
  top = "normal",
  bottom = "normal",
  children,
}) {
  const Borders = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    border-left: ${borderStyle(left)};
    border-right: ${borderStyle(right)};
    border-top: ${borderStyle(top)};
    border-bottom: ${borderStyle(bottom)};
    box-sizing: border-box;
  `

  return <Borders>{children}</Borders>
}
CellBorders.propTypes = propTypes

function borderStyle(type) {
  switch (type) {
    case "bold":
      return "3px solid black"
    case "normal":
      return "1px solid black"
    default:
      return "none"
  }
}

/**
 * Builds the props to pass to the CellBorders component for a simple grid with normal borders.
 *
 * @param i row number of the cell
 * @param j column number of the cell
 * @returns {{top: string, left: string, bottom: string, right: string}} the props to pass to the component
 */
export function simpleGridProps(i, j) {
  return {
    top: i === 0 ? "normal" : "none",
    left: j === 0 ? "normal" : "none",
    bottom: "normal",
    right: "normal",
  }
}

/**
 * Builds the props to pass to the CellBorders component for asudoku grid.
 *
 * @param i row number of the cell
 * @param j column number of the cell
 * @returns {{top: string, left: string, bottom: string, right: string}} the props to pass to the component
 */
export function sudokuGridProps(i, j) {
  return {
    top: i === 0 ? "bold" : "none",
    left: j === 0 ? "bold" : "none",
    bottom: i % 3 === 2 ? "bold" : "normal",
    right: j % 3 === 2 ? "bold" : "normal",
  }
}

import React from "react"
import { fireEvent, render } from "@testing-library/react"
import ShadingCell from "./ShadingCell"

describe("ShadingCell", () => {
  it.each([
    ["UNKNOWN", "SHADED"],
    ["SHADED", "UNSHADED"],
    ["UNSHADED", "UNKNOWN"],
  ])(
    "Iterates between the three possible values in order",
    (value, expected) => {
      const onChangeSpy = jest.fn()
      const { container } = render(
        <ShadingCell value={value} onChange={onChangeSpy} />,
      )
      fireEvent.click(container.firstChild)
      expect(onChangeSpy).toHaveBeenCalledWith(expected)
    },
  )
})

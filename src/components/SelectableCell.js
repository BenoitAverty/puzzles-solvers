import React from "react"
import PropTypes from "prop-types"
import styled from "@emotion/styled"

import { noop } from "utils"

const propTypes = {
  /** Boolean that indicates if the cell is selected or not. */
  selected: PropTypes.bool,
  /** Callback called when the cell is selected (clicked on) */
  onSelect: PropTypes.func,
  /** Callback called when the cell is added to the selection. */
  onAddToSelection: PropTypes.func,
  /** Content of the cell. */
  children: PropTypes.node,
}

/**
 * Cell that can be selected. It signals to its parent when it's selected (clicked on) or
 * added to a selection (mouse enter with left button pressed). Selection status is shown with
 * a background color change.
 */
export default function SelectableCell({
  selected = false,
  onSelect = noop,
  onAddToSelection = noop,
  children = null,
}) {
  return (
    <StyledCellRoot
      selected={selected}
      data-debug={`selected:${selected}`}
      onMouseDown={() => onSelect()}
      onMouseEnter={e => {
        if (e.buttons === 1) {
          onAddToSelection()
        }
      }}
    >
      {children}
    </StyledCellRoot>
  )
}
SelectableCell.propTypes = propTypes

const StyledCellRoot = styled.div`
  background-color: ${props => (props.selected ? "lightgray" : "transparent")};
  width: 100%;
  height: 100%;
  user-select: none;
`

import React from "react"
import { select } from "@storybook/addon-knobs"

import CellBorders, { simpleGridProps } from "./CellBorders"

export default {
  title: "3. Low level components/CellBorders",
  component: CellBorders,
}

const borderType = label => select(label, ["none", "normal", "bold"], "normal")
export const Playground = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px" }}>
    <CellBorders
      left={borderType("Type of left border")}
      right={borderType("Type of right border")}
      top={borderType("Type of top border")}
      bottom={borderType("Type of bottom border")}
    >
      🚀
    </CellBorders>
  </div>
)

export const NormalBorders = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px" }}>
    <CellBorders left="normal" right="normal" top="normal" bottom="normal">
      🚀
    </CellBorders>
  </div>
)

export const BoldBorders = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px" }}>
    <CellBorders left="bold" right="bold" top="bold" bottom="bold">
      🚀
    </CellBorders>
  </div>
)

export const MixedBorders = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px" }}>
    <CellBorders left="normal" right="none" top="bold" bottom="bold">
      🚀
    </CellBorders>
  </div>
)

export const SimpleGridBorders = () => (
  <div
    style={{ display: "inline-grid", gridTemplate: "50px 50px / 50px 50px" }}
  >
    <CellBorders {...simpleGridProps(0, 0)}>🤩</CellBorders>
    <CellBorders {...simpleGridProps(0, 1)}>🤩</CellBorders>
    <CellBorders {...simpleGridProps(1, 0)}>🤩</CellBorders>
    <CellBorders {...simpleGridProps(1, 1)}>🤩</CellBorders>
  </div>
)

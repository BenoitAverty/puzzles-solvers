import { simpleGridProps, sudokuGridProps } from "./CellBorders"
import { range } from "utils"

describe("simpleGridProps", () => {
  it.each([[0, 0], [0, 1], [1, 0], [1, 1]])(
    "Returns borders that make a grid",
    (i, j) => {
      expect(simpleGridProps(i, j)).toMatchSnapshot()
    },
  )
})

describe("sudokuGridProps", () => {
  const sudokuCoordinates = range(0, 9).map(i => range(0, 9).map(j => [i, j]))
  it.each(sudokuCoordinates)("Returns borders that make a grid", (i, j) => {
    expect(sudokuGridProps(i, j)).toMatchSnapshot()
  })
})

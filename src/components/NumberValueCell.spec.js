import React from "react"
import { render, fireEvent } from "@testing-library/react"

import NumberValueCell from "./NumberValueCell"

describe("NumberValueCell", () => {
  it("Changes value on key strokes", () => {
    const onChangeSpy = jest.fn()
    render(<NumberValueCell onChange={onChangeSpy} />)

    fireEvent.keyDown(document, { code: "Digit5" })

    expect(onChangeSpy).toHaveBeenCalledWith({ main: 5 })
  })

  it("Does not change value when disabled", () => {
    const onChangeSpy = jest.fn()
    render(<NumberValueCell disabled onChange={onChangeSpy} />)

    fireEvent.keyDown(document, { code: "Digit5" })

    expect(onChangeSpy).not.toHaveBeenCalled()
  })

  it("Does not change value on unknown keystrokes", () => {
    const onChangeSpy = jest.fn()
    render(<NumberValueCell disabled onChange={onChangeSpy} />)

    fireEvent.keyDown(document, { code: "KeyD" })

    expect(onChangeSpy).not.toHaveBeenCalled()
  })

  it("Changes corner pencil marks with shift key", () => {
    const onChangeSpy = jest.fn()
    render(<NumberValueCell onChange={onChangeSpy} />)

    fireEvent.keyDown(document, { code: "Digit5", shiftKey: true })

    expect(onChangeSpy).toHaveBeenCalledWith({ corner: [5] })
  })

  it("Changes center pencil marks with ctrl key", () => {
    const onChangeSpy = jest.fn()
    render(<NumberValueCell onChange={onChangeSpy} />)

    fireEvent.keyDown(document, { code: "Digit5", ctrlKey: true })

    expect(onChangeSpy).toHaveBeenCalledWith({ center: [5] })
  })

  it("Handles multiple values in pencil marks", () => {
    const onChangeSpy = jest.fn()
    render(
      <NumberValueCell
        onChange={onChangeSpy}
        value={{ center: [4, 5], corner: [6] }}
      />,
    )

    fireEvent.keyDown(document, { code: "Digit6", ctrlKey: true })
    fireEvent.keyDown(document, { code: "Digit7", shiftKey: true })

    expect(onChangeSpy).toHaveBeenCalledWith({ center: [4, 5, 6], corner: [6] })
    expect(onChangeSpy).toHaveBeenCalledWith({ center: [4, 5], corner: [6, 7] })
  })

  it("Removes already present pencil marks", () => {
    const onChangeSpy = jest.fn()
    render(
      <NumberValueCell
        onChange={onChangeSpy}
        value={{ center: [4, 5], corner: [6] }}
      />,
    )

    fireEvent.keyDown(document, { code: "Digit4", ctrlKey: true })
    fireEvent.keyDown(document, { code: "Digit6", shiftKey: true })

    expect(onChangeSpy).toHaveBeenCalledWith({ center: [4, 5], corner: [] })
  })

  it("Clears main value if present", () => {
    const onChangeSpy = jest.fn()
    render(
      <NumberValueCell
        onChange={onChangeSpy}
        value={{ center: [4, 5], corner: [6], main: 5 }}
      />,
    )

    fireEvent.keyDown(document, { code: "Backspace" })

    expect(onChangeSpy).toHaveBeenCalledWith({
      center: [4, 5],
      corner: [6],
      main: null,
    })
  })

  it("Clears pencil marks if there is no main value", () => {
    const onChangeSpy = jest.fn()
    render(
      <NumberValueCell
        onChange={onChangeSpy}
        value={{ center: [4, 5], corner: [6] }}
      />,
    )

    fireEvent.keyDown(document, { code: "Backspace" })

    expect(onChangeSpy).toHaveBeenCalledWith({
      center: [],
      corner: [],
      main: null,
    })
  })

  it("Shows only main value if there is one", () => {
    const { container } = render(
      <NumberValueCell value={{ center: [4, 5], corner: [6], main: 1 }} />,
    )

    expect(container.textContent).toBe("1")
  })
})

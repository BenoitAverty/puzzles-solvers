import React from "react"

import MultiClueCell from "./MultiClueCell"

export default {
  title: "3. Low level components/MultiClueCell",
  component: MultiClueCell,
}

export const OneClue = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <MultiClueCell clues={[5]} />
  </div>
)

export const TwoClues = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <MultiClueCell clues={[1, 2]} />
  </div>
)

export const ThreeClues = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <MultiClueCell clues={[1, 2, 3]} />
  </div>
)

export const FourClues = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <MultiClueCell clues={[1, 2, 3, 4]} />
  </div>
)

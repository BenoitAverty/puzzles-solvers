import React from "react"
import { action } from "@storybook/addon-actions"

import NumberValueCell from "./NumberValueCell"
import { boolean } from "@storybook/addon-knobs"

export default {
  title: "3. Low level components/NumberValueCell",
  component: NumberValueCell,
}

const logChange = action("Value changed")

export const Simple = () => {
  const [value, setValueState] = React.useState({ main: 5 })

  return (
    // The div has a size that makes the cell behave like it was in a grid
    <div style={{ width: "50px", height: "50px", border: "1px solid" }}>
      <NumberValueCell
        value={value}
        disabled={boolean("Disabled", false)}
        onChange={v => {
          logChange(v)
          setValueState(v)
        }}
      />
    </div>
  )
}

export const PencilMarks = () => {
  const [value, setValueState] = React.useState({
    center: [5, 6],
    corner: [1, 2, 3, 4],
  })

  return (
    // The div has a size that makes the cell behave like it was in a grid
    <div style={{ width: "50px", height: "50px", border: "1px solid" }}>
      <NumberValueCell
        value={value}
        disabled={boolean("Disabled", false)}
        onChange={v => {
          logChange(v)
          setValueState(v)
        }}
      />
    </div>
  )
}

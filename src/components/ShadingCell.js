import React from "react"
import PropTypes from "prop-types"
import styled from "@emotion/styled"

import { noop } from "utils"

const propTypes = {
  /**
   * Value to display. If not provided, defaults to "UNKNOWN"
   */
  value: PropTypes.oneOf(["UNKNOWN", "SHADED", "UNSHADED"]),
  /** callback for the value change. called with one parameter that has the same possible values as the value prop. */
  onChange: PropTypes.func,
}

/**
 * Cell that can be shaded or unshaded.
 *
 * Click the cell to toggle state between unknown/shaded/unshaded.
 */
export default function ShadingCell({
  value: cellValue = "UNKNOWN",
  onChange = noop,
}) {
  const nextValueMapping = {
    UNKNOWN: "SHADED",
    SHADED: "UNSHADED",
    UNSHADED: "UNKNOWN",
  }

  return (
    <StyledCellRoot
      shaded={cellValue === "SHADED"}
      onClick={() => onChange(nextValueMapping[cellValue])}
      data-debug={`shaded:${cellValue}`}
    >
      {cellValue === "UNSHADED" ? <Dot /> : null}
    </StyledCellRoot>
  )
}
ShadingCell.propTypes = propTypes

const StyledCellRoot = styled.div`
  background-color: ${props => (props.shaded ? "#555" : "transparent")};
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  user-select: none;
`

const Dot = styled.div`
  width: 25%;
  height: 25%;
  background-color: #aaa;
  border-radius: 50%;
`

import React from "react"
import PropTypes from "prop-types"
import { integerBetween } from "../customPropTypes"
import styled from "@emotion/styled"

const propTypes = {
  clues: PropTypes.arrayOf(integerBetween(0, 9)),
}
/**
 * Cell used to display up to 4 numbers while being the most efficient possible with clue space.
 */
export default function MultiClueCell({ clues }) {
  return (
    <StyledCellRoot>
      {clues.map((c, i) => (
        <StyledClueContainer key={i} n={clues.length} index={i}>
          {c}
        </StyledClueContainer>
      ))}
    </StyledCellRoot>
  )
}
MultiClueCell.propTypes = propTypes

// Mapping of props based on number of clues and position in the array
const propsMapping = [
  [{ height: "100%", fontSize: "1.5em" }],
  [
    { top: "10%", left: "10%", fontSize: "1.3em" },
    { bottom: "10%", right: "10%", fontSize: "1.3em" },
  ],
  [
    { top: "10%", left: "10%", fontSize: "1.1em" },
    { top: "10%", right: "10%", fontSize: "1.1em" },
    { bottom: "10%", width: "100%", fontSize: "1.1em" },
  ],
  [
    { top: "0%", width: "100%" },
    { left: "5%", height: "100%" },
    { right: "5%", height: "100%" },
    { bottom: "0%", width: "100%" },
  ],
]
const StyledCellRoot = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`
const StyledClueContainer = styled.div(props => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: props.n > 1 ? "absolute" : "normal",
  ...propsMapping[props.n - 1][props.index],
}))

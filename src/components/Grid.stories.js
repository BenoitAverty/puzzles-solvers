import React from "react"
import { number } from "@storybook/addon-knobs"

import Grid from "./Grid"

export default {
  title: "3. Low level components/Grid",
  component: Grid,
}

export const Playground = () => (
  <Grid
    rows={number("Number of rows", 5, { step: 1 })}
    columns={number("Number of columns", 5, { step: 1 })}
    cellWidth={number("Cell width", 45)}
    cellHeight={number("Cell height", 45)}
  >
    {(i, j) => <p>{`${i},${j}`}</p>}
  </Grid>
)

export const IrregularRatio = () => (
  <Grid rows={2} columns={7}>
    {(i, j) => <p>{`${i},${j}`}</p>}
  </Grid>
)

export const RectangularCells = () => (
  <Grid rows={5} columns={5} cellWidth={100} cellHeight={30}>
    {(i, j) => <p>{`${i},${j}`}</p>}
  </Grid>
)

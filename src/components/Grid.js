import React from "react"
import styled from "@emotion/styled"
import PropTypes from "prop-types"
import { mapGridCoordinates } from "utils"

const propTypes = {
  /** Number of rows in the grid */
  rows: PropTypes.number,
  /** Number of columns in the grid */
  columns: PropTypes.number,
  /** Width of cells in pixels */
  cellWidth: PropTypes.number,
  /** Height of cells in pixels */
  cellHeight: PropTypes.number,
  /** Function called to render a cell.
   *
   * Called with i and j parameters (rows and columns index starting from 0).
   *
   * Must return something that React can render.
   */
  children: PropTypes.func.isRequired,
}

/**
 * Component responsible of placing things in a grid layout.
 *
 * Apart from the layout, nothing is displayed by this component. Instead, it calls its `children` prop with the
 * coordinates in the grid.
 */
export default function Grid({
  rows = 9,
  columns = 9,
  cellWidth = 50,
  cellHeight = 50,
  children,
}) {
  // Generate a matrix corresponding to the cells of the grid.
  // Each item is an object with the coordinates of the cell.
  const matrixOfCoordinates = mapGridCoordinates(rows, columns, (i, j) => ({
    i,
    j,
  }))

  // Function that generates the react element for one row
  const buildRowCells = row =>
    row.map(({ i, j }) => (
      <div key={columns * i + j} data-testid={`cell-${i}-${j}`}>
        {children(i, j)}
      </div>
    ))

  return (
    <StyledGridRoot
      rows={rows}
      columns={columns}
      cellWidth={cellWidth}
      cellHeight={cellHeight}
    >
      {matrixOfCoordinates.map(buildRowCells)}
    </StyledGridRoot>
  )
}
Grid.propTypes = propTypes

// Grid root component (styles)
const StyledGridRoot = styled.div`
  display: inline-grid;
  grid-template-columns: repeat(
    ${props => props.columns},
    ${props => props.cellWidth}px
  );
  grid-template-rows: repeat(
    ${props => props.rows},
    ${props => props.cellHeight}px
  );
`

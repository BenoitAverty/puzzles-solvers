import React from "react"
import ShadingCell from "./ShadingCell"

export default {
  title: "3. Low level components/ShadingCell",
  component: ShadingCell,
}

export const Unknown = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <ShadingCell value="UNKNOWN" />
  </div>
)

export const Shaded = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <ShadingCell value="SHADED" />
  </div>
)

export const MultipleShaded = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <>
    <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
      <ShadingCell value="SHADED" />
    </div>
    <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
      <ShadingCell value="SHADED" />
    </div>
  </>
)

export const Unshaded = () => (
  // The div has a size that makes the cell behave like it was in a grid
  <div style={{ width: "50px", height: "50px", border: "1px solid black" }}>
    <ShadingCell value="UNSHADED" />
  </div>
)

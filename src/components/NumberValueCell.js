import React from "react"
import PropTypes from "prop-types"
import styled from "@emotion/styled"

import useSolverKeyboardEvents from "hooks/useSolverKeyboardEvents"
import { integerBetween, optionalIntegerBetween } from "customPropTypes"
import { noop, toggleInArray } from "utils"

const propTypes = {
  /**
   * Value to display. pass null or empty object for empty value.
   * This component only works in "controlled" mode (it has no internal state
   * for the value, you must use this prop for the component to display a number)
   */
  value: PropTypes.shape({
    main: optionalIntegerBetween(1, 9),
    center: PropTypes.arrayOf(integerBetween(1, 9)),
    corner: PropTypes.arrayOf(integerBetween(1, 9)),
  }),
  /** callback for the value change. called with one parameter that has the same shape as the value prop. */
  onChange: PropTypes.func,
  /** Pass true to disable the cell (it won't call onChange on keystrokes) */
  disabled: PropTypes.bool,
}

/**
 * Cell that displays a number value between 1 and 9, with pencil marks support.
 *
 * Use 1/9 keys to input numbers, shift key for corner pencil marks, ctrl key for
 * center pencil marks.
 *
 * Use backspace to clear the values.
 */
export default function NumberValueCell({
  value: cellValue = {},
  onChange = noop,
  disabled = false,
}) {
  useSolverKeyboardEvents(e => {
    if (!disabled) {
      if (e.value === null && cellValue.main) {
        // If clearing a cell with a main value, keep pencil marks
        onChange({ ...cellValue, main: e.value })
      } else if (e.value === null) {
        // Clear pencil marks if there is no main value
        onChange({ main: null, center: [], corner: [] })
      } else if (e.nativeEvent.shiftKey) {
        // Setting or removing a corner mark
        onChange({
          ...cellValue,
          corner: toggleInArray(cellValue.corner, e.value),
        })
      } else if (e.nativeEvent.ctrlKey) {
        // Setting or removing a center mark
        onChange({
          ...cellValue,
          center: toggleInArray(cellValue.center, e.value),
        })
      } else {
        // Set main value
        onChange({ ...cellValue, main: e.value })
      }
    }
  })
  const centerPencilMark =
    cellValue.center && cellValue.center.length > 0 ? (
      <PencilMark>{cellValue.center.join("")}</PencilMark>
    ) : null

  const cornerPencilMarks =
    cellValue.corner && cellValue.corner.length > 0 ? (
      <>
        <TopLeft>
          <PencilMark>{cellValue.corner[0]}</PencilMark>
        </TopLeft>
        <TopRight>
          <PencilMark>{cellValue.corner[1] || null}</PencilMark>
        </TopRight>
        <BottomLeft>
          <PencilMark>{cellValue.corner[2] || null}</PencilMark>
        </BottomLeft>
        <BottomRight>
          <PencilMark>{cellValue.corner[3] || null}</PencilMark>
        </BottomRight>
      </>
    ) : null

  const pencilMarks = (
    <>
      {cornerPencilMarks}
      {centerPencilMark}
    </>
  )
  return (
    <StyledCellRoot>
      {cellValue.main ? <MainNumber>{cellValue.main}</MainNumber> : pencilMarks}
    </StyledCellRoot>
  )
}
NumberValueCell.propTypes = propTypes

const TopLeft = styled.div`
  position: absolute;
  top: 0;
  left: 0;
`
const TopRight = styled.div`
  position: absolute;
  top: 0;
  right: 0;
`
const BottomLeft = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
`
const BottomRight = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
`

const PencilMark = styled.span`
  font-size: 1em;
  color: gray;
`

const MainNumber = styled.span`
  color: blue;
  font-size: 1.5em;
`

const StyledCellRoot = styled.div`
  background-color: transparent;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`

import React from "react"
import { render, fireEvent } from "@testing-library/react"
import ClassicSudokuSetter from "./ClassicSudokuSetter"

describe("ClassicSudokuSetter", () => {
  it("Is initially empty", () => {
    const { container } = render(<ClassicSudokuSetter />)

    expect(container).toHaveTextContent(/^$/)
  })

  it("Converts grid into a gridString", () => {
    let value = ""
    const { getByTestId } = render(
      <ClassicSudokuSetter onChange={v => (value = v)} />,
    )

    fireEvent.mouseDown(getByTestId("cell-0-0").firstChild)
    fireEvent.keyDown(document, { code: "Digit5" })
    fireEvent.mouseDown(getByTestId("cell-1-1").firstChild)
    fireEvent.mouseEnter(getByTestId("cell-1-2").firstChild, { buttons: 1 })
    fireEvent.keyDown(document, { code: "Digit6" })
    fireEvent.mouseDown(getByTestId("cell-7-7").firstChild)
    fireEvent.keyDown(document, { code: "Digit3" })
    fireEvent.mouseDown(getByTestId("cell-3-1").firstChild)
    fireEvent.keyDown(document, { code: "Digit7" })
    fireEvent.mouseDown(getByTestId("cell-5-4").firstChild)
    fireEvent.keyDown(document, { code: "Digit9" })
    fireEvent.mouseDown(getByTestId("cell-1-1").firstChild)
    fireEvent.keyDown(document, { code: "Backspace" })

    expect(value).toBe(
      "5__________6________________7____________________9____________________3__________",
    )
  })
})

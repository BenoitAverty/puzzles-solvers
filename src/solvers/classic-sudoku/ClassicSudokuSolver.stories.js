import React from "react"

import ClassicSudokuSolver from "./ClassicSudokuSolver"

export default {
  title: "1. Solvers/ClassicSudokuSolver",
  component: ClassicSudokuSolver,
}

export const EasySudoku = () => {
  return (
    <ClassicSudokuSolver
      gridString={`
        _____4_28
        4_6_____5
        1___3_6__
        ___3_1___
        _87___14_
        ___7_9___
        __2_1___3
        9_____5_7
        67_4_____`}
    />
  )
}

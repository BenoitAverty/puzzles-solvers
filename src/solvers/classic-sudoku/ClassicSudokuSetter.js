import React from "react"
import PropTypes from "prop-types"

import { noop } from "utils"
import Grid from "components/Grid"
import ClassicSudokuCell from "./ClassicSudokuCell"
import useGridState from "hooks/useGridState"
import useClickOutside from "hooks/useClickOutside"

const propTypes = {
  /**
   * Callback called each time the grid changes.
   *
   * It receives one parameter that can be fed to ClassicSudokuSolver component.
   */
  onChange: PropTypes.func,
}

function toGridString(userValues) {
  return userValues
    .flat()
    .map(value => value.main)
    .map(value => value || "_")
    .join("")
}

/**
 * Component allowing a user to enter a classic sudoku grid.
 *
 * Select cells and use the digit keys to enter clues.
 */
export default function ClassicSudokuSetter({ onChange = noop }) {
  const { grid: userValues, get: getValue, set: setCellValue } = useGridState(9, 9, {
    main: null,
  })

  React.useEffect(() => {
    onChange(toGridString(userValues))
  }, [userValues, onChange])
  const {
    get: isSelected,
    set: setSelected,
    reset: resetSelection,
  } = useGridState(9, 9, false)

  const clickOutsideRef = useClickOutside(resetSelection)

  return (
    <div style={{ display: "inline-block" }} ref={clickOutsideRef}>
      <Grid columns={9} rows={9}>
        {(i, j) => (
          <ClassicSudokuCell
            i={i}
            j={j}
            isSelected={isSelected}
            onSelect={() => {
              resetSelection()
              setSelected(i, j, true)
            }}
            onAddToSelection={() => setSelected(i, j, true)}
            getValue={getValue}
            onChange={s => {
              return setCellValue(i, j, s)
            }}
          />
        )}
      </Grid>
    </div>
  )
}
ClassicSudokuSetter.propTypes = propTypes

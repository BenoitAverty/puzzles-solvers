import React from "react"

import ClassicSudokuSetter from "./ClassicSudokuSetter"
import { action } from "@storybook/addon-actions"

export default {
  title: "2. Setters/ClassicSudokuSetter",
  component: ClassicSudokuSetter,
}

export const Playground = () => {
  return <ClassicSudokuSetter onChange={action("Grid Changed")} />
}

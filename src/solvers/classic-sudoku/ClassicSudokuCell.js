import SelectableCell from "../../components/SelectableCell"
import CellBorders, { sudokuGridProps } from "../../components/CellBorders"
import MultiClueCell from "../../components/MultiClueCell"
import NumberValueCell from "../../components/NumberValueCell"
import PropTypes from "prop-types"
import React from "react"

// Internal API !
export default function ClassicSudokuCell({
  isSelected,
  onSelect,
  onAddToSelection,
  getValue,
  initialGrid,
  onChange,
  i,
  j,
}) {
  return (
    <SelectableCell
      selected={isSelected(i, j)}
      onSelect={onSelect}
      onAddToSelection={onAddToSelection}
    >
      <CellBorders {...sudokuGridProps(i, j)}>
        {initialGrid && initialGrid[i][j].main ? (
          <MultiClueCell clues={[initialGrid[i][j].main]} />
        ) : (
          <NumberValueCell
            value={getValue(i, j)}
            disabled={!isSelected(i, j)}
            onChange={onChange}
          />
        )}
      </CellBorders>
    </SelectableCell>
  )
}

ClassicSudokuCell.propTypes = {
  isSelected: PropTypes.any.isRequired,
  i: PropTypes.any.isRequired,
  j: PropTypes.any.isRequired,
  onSelect: PropTypes.func.isRequired,
  onAddToSelection: PropTypes.func.isRequired,
  initialGrid: PropTypes.any, // Initial Grid is not required : if absent, no clues are given.
  getValue: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
}

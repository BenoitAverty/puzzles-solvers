import React from "react"
import PropTypes from "prop-types"

import Grid from "components/Grid"
import { range } from "utils"
import useClickOutside from "hooks/useClickOutside"
import useGridState from "hooks/useGridState"
import ClassicSudokuCell from "./ClassicSudokuCell"

// utils
const isSudokuStringCell = char =>
  char === "_" || (Number(char) >= 1 && Number(char) <= 9)

const propTypes = {
  /**
   * A string representation of the initial grid.
   *
   * Each character represents a cell from left to right then top to bottom. Use '_' for an empty cell.
   * Blank chars will be ignored.
   *
   * Example :
   * `_____4_28
   * 4_6_____5
   * 1___3_6__
   * ___3_1___
   * _87___14_
   * ___7_9___
   * __2_1___3
   * 9_____5_7
   * 67_4_____`
   */
  gridString: PropTypes.string.isRequired,
}

/**
 * Classic Sudoku Solver.
 *
 * Features:
 * <ul>
 *   <li>Select one or more cells</li>
 *   <li>Enter value with number keys</li>
 *   <li>Enter pencil marks with shift/ctrl keys</li>
 * </ul>
 */
export default function ClassicSudokuSolver({ gridString }) {
  const initialGrid = React.useMemo(() => {
    const flatGrid = [...gridString]
      .filter(isSudokuStringCell)
      .map(Number)
      .map(n => (isNaN(n) ? null : n))
      .map(val => ({ main: val }))

    return range(0, 9).map(i => flatGrid.slice(i * 9, i * 9 + 9))
  }, [gridString])

  // Values and selection.
  const { get: getValue, set: setCellValue } = useGridState(9, 9, {
    main: null,
  })
  const {
    get: isSelected,
    set: setSelected,
    reset: resetSelection,
  } = useGridState(9, 9, false)

  const clickOutsideRef = useClickOutside(resetSelection)

  return (
    <div style={{ display: "inline-block" }} ref={clickOutsideRef}>
      <Grid rows={9} columns={9}>
        {(i, j) => (
          <ClassicSudokuCell
            i={i}
            j={j}
            isSelected={isSelected}
            onSelect={() => {
              resetSelection()
              setSelected(i, j, true)
            }}
            onAddToSelection={() => setSelected(i, j, true)}
            initialGrid={initialGrid}
            getValue={getValue}
            onChange={s => {
              return setCellValue(i, j, s)
            }}
          />
        )}
      </Grid>
    </div>
  )
}
ClassicSudokuSolver.propTypes = propTypes

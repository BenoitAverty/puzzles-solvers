import React from "react"
import { fireEvent, render } from "@testing-library/react"
import { EasySudoku } from "./ClassicSudokuSolver.stories"

describe("ClassicSudokuSolver", () => {
  let wrapper
  beforeEach(() => {
    wrapper = render(<EasySudoku />)
  })

  it("Contains given numbers when initially rendered", () => {
    // This doesn't assert the position of the numbers, nor the position of blank cells.
    // TODO: make a custom assertion that matches grid content more precisely.
    expect(wrapper.container).toHaveTextContent(/^42846513631871479213957674$/)
  })

  it("Allows changing the value of cells", () => {
    const cell = wrapper.getByTestId("cell-0-0")

    // Cell initially empty
    expect(cell).toHaveTextContent(/^$/)

    // Selecting the cell and typing a digit should change its value
    fireEvent.mouseDown(cell.firstChild)
    fireEvent.keyDown(document, {
      code: "Digit5",
    })

    expect(cell).toHaveTextContent("5")
  })

  it("Allows entering pencil marks into cells", () => {
    const cell = wrapper.getByTestId("cell-8-4")

    // Cell initially empty
    expect(cell).toHaveTextContent(/^$/)

    // Selecting the cell and typing a digit should change its value
    fireEvent.mouseDown(cell.firstChild)
    fireEvent.keyDown(document, {
      code: "Digit5",
      shiftKey: true,
    })
    fireEvent.keyDown(document, {
      code: "Digit6",
      shiftKey: true,
    })
    fireEvent.keyDown(document, {
      code: "Digit6",
      ctrlKey: true,
    })

    expect(cell).toHaveTextContent("566")
  })

  it("Allows editing several cells at once", () => {
    const cell1 = wrapper.getByTestId("cell-1-1")
    const cell2 = wrapper.getByTestId("cell-2-1")

    // Selecting the cell and typing a digit should change its value
    fireEvent.mouseDown(cell1.firstChild)
    fireEvent.mouseEnter(cell2.firstChild, { buttons: 1 })
    fireEvent.keyDown(document, {
      code: "Digit3",
      shiftKey: true,
    })

    expect(cell1).toHaveTextContent("3")
    expect(cell2).toHaveTextContent("3")
  })

  it("Unselects all cells when clicked outside", () => {
    const cell1 = wrapper.getByTestId("cell-1-1")
    const cell2 = wrapper.getByTestId("cell-2-1")

    // Selecting two cells
    fireEvent.mouseDown(cell1.firstChild)
    fireEvent.mouseEnter(cell2.firstChild, { buttons: 1 })

    // Clicking outside then typing a number
    fireEvent.mouseDown(document)
    fireEvent.keyDown(document, {
      code: "Digit3",
    })

    // Cells should stay empty
    expect(cell1).toHaveTextContent(/^$/)
    expect(cell2).toHaveTextContent(/^$/)
  })
})

import React from "react"
import { render, fireEvent } from "@testing-library/react"
import TapaSetter from "./TapaSetter"

describe("TapaSetter", () => {
  it("Is initially empty", () => {
    const { container } = render(<TapaSetter rows={8} columns={8} />)

    expect(container).toHaveTextContent(/^$/)
  })

  it("Converts grid into a gridString", () => {
    let value = ""
    const { getByTestId, container } = render(
      <TapaSetter rows={8} columns={8} onChange={v => (value = v)} />,
    )

    fireEvent.mouseDown(getByTestId("cell-0-0").firstChild)
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.keyDown(document, { code: "Digit2" })
    fireEvent.mouseDown(getByTestId("cell-6-6").firstChild)
    fireEvent.keyDown(document, { code: "Digit3" })
    fireEvent.keyDown(document, { code: "Digit3" })
    fireEvent.mouseDown(getByTestId("cell-1-1").firstChild)
    fireEvent.mouseEnter(getByTestId("cell-1-2").firstChild, { buttons: 1 })
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.mouseDown(getByTestId("cell-1-1").firstChild)
    fireEvent.keyDown(document, { code: "Backspace" })

    expect(value).toBe(
      "12,_,_,_,_,_,_,_,_,_,1111,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,33,_,_,_,_,_,_,_,_,_",
    )
    expect(container).toHaveTextContent(/^12111133$/)
  })

  it("Doesn't allow more than 4 clues in a single cell", () => {
    let value = ""
    const { getByTestId, container } = render(
      <TapaSetter rows={2} columns={2} onChange={v => (value = v)} />,
    )

    fireEvent.mouseDown(getByTestId("cell-0-0").firstChild)
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.keyDown(document, { code: "Digit2" })
    fireEvent.keyDown(document, { code: "Digit3" })
    fireEvent.keyDown(document, { code: "Digit4" })
    fireEvent.keyDown(document, { code: "Digit5" })

    expect(value).toBe("1235,_,_,_")
    expect(container).toHaveTextContent(/^1235$/)
  })

  it("Keeps the grid anchored to the top-left when resized up", () => {
    let value = ""
    const { getByTestId, container, rerender } = render(
      <TapaSetter rows={4} columns={4} onChange={v => (value = v)} />,
    )

    fireEvent.mouseDown(getByTestId("cell-2-1").firstChild)
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.mouseDown(getByTestId("cell-1-2").firstChild)
    fireEvent.keyDown(document, { code: "Digit2" })

    rerender(<TapaSetter rows={5} columns={5} onChange={v => (value = v)} />)

    expect(container).toHaveTextContent(/^21$/)
    expect(value).toBe("_,_,_,_,_,_,_,2,_,_,_,1,_,_,_,_,_,_,_,_,_,_,_,_,_")
  })

  it("Keeps the grid anchored to the top-left when resized down", () => {
    let value = ""
    const { getByTestId, container, rerender } = render(
      <TapaSetter rows={4} columns={4} onChange={v => (value = v)} />,
    )

    fireEvent.mouseDown(getByTestId("cell-1-1").firstChild)
    fireEvent.keyDown(document, { code: "Digit1" })
    fireEvent.mouseDown(getByTestId("cell-3-3").firstChild)
    fireEvent.keyDown(document, { code: "Digit2" })

    rerender(<TapaSetter rows={3} columns={3} onChange={v => (value = v)} />)

    expect(container).toHaveTextContent(/^1$/)
    expect(value).toBe("_,_,_,_,1,_,_,_,_")
  })
})

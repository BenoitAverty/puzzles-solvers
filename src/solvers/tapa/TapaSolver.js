import React from "react"
import PropTypes from "prop-types"

import Grid from "components/Grid"
import CellBorders, { simpleGridProps } from "components/CellBorders"
import ShadingCell from "components/ShadingCell"
import MultiClueCell from "components/MultiClueCell"
import { range } from "utils"
import useGridState from "hooks/useGridState"

const propTypes = {
  /**
   * A string representation of the initial grid.
   *
   * Each cell is represented by a group of chars separated by commas :
   * <ul>
   *   <li>`_` for an empty cell</li>
   *   <li>digits between 1 and 8 for clue cells. there can be up to 4 digits in a single cell</li>
   * </ul>
   *
   * The number of cells must be consistent with the rows/columns props.
   *
   * Example :
   * `3,_,_,111,_,_,_,
   * _,_,_,_,_,_,3,
   * _,_,15,_,_,_,_,
   * _,_,_,_,_,_,_,
   * _,_,_,_,7,_,_,
   * 4,_,_,_,_,_,
   * _,_,_,13,_,_,11`
   */
  gridString: PropTypes.string.isRequired,
  /** Number of rows in the grid */
  rows: PropTypes.number.isRequired,
  /** Number of columns in the grid */
  columns: PropTypes.number.isRequired,
}

/**
 * Tapa solver
 *
 * Features:
 * <ul>
 *   <li>Toggle cell state between shaded/unshaded/unknown</li>
 * </ul>
 */
export default function TapaSolver({ gridString, rows, columns }) {
  const clues = React.useMemo(() => {
    const flatGrid = gridString
      .split(",")
      .map(token => token.trim())
      .map(cellContent =>
        cellContent === "_" ? null : [...cellContent].map(Number),
      )

    if (flatGrid.length !== rows * columns) {
      throw new Error(
        "The given gridString is inconsistent with the number of rows and columns.",
      )
    }

    return range(0, rows).map(i =>
      flatGrid.slice(i * columns, i * columns + columns),
    )
  }, [gridString, rows, columns])

  // Values and selection.
  const { get: userValue, set: setCellValue, reset: resetCells } = useGridState(
    rows,
    columns,
    "UNKNOWN",
  )

  // Reset the user values when the tapa changes
  React.useEffect(() => {
    resetCells()
  }, [rows, columns, gridString, resetCells])

  return (
    <div style={{ display: "inline-block" }}>
      <Grid rows={rows} columns={columns}>
        {(i, j) => (
          <CellBorders {...simpleGridProps(i, j)}>
            {clues[i][j] ? (
              <MultiClueCell clues={clues[i][j]} />
            ) : (
              <ShadingCell
                value={userValue(i, j)}
                onChange={v => setCellValue(i, j, v)}
              />
            )}
          </CellBorders>
        )}
      </Grid>
    </div>
  )
}
TapaSolver.propTypes = propTypes

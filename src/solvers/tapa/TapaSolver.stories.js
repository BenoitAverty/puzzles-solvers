import React from "react"

import TapaSolver from "./TapaSolver"

export default {
  title: "1. Solvers/TapaSolver",
  component: TapaSolver,
}

export const SmallTapa = () => {
  return (
    <TapaSolver
      rows={7}
      columns={7}
      gridString={`
        3,_,_,111,_,_,_,
        _,_,_,_,_,_,3,
        _,_,15,_,_,_,_,
        _,_,_,_,_,_,_,
        _,_,_,_,7,_,_,
        4,_,_,_,_,_,_,
        _,_,_,13,_,_,11`}
    />
  )
}

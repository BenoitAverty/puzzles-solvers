import React from "react"
import { fireEvent, render } from "@testing-library/react"

import { SmallTapa } from "solvers/tapa/TapaSolver.stories"
import TapaSolver from "./TapaSolver"
import { noop } from "utils"

describe("TapaSolver", () => {
  it("Contains clues when initially rendered", () => {
    const { getByTestId } = render(<SmallTapa />)

    // This doesn't assert that the other cells are blank.
    // TODO: make a custom assertion that matches grid content more precisely.
    expect(getByTestId("cell-0-0")).toHaveTextContent(/^3$/)
    expect(getByTestId("cell-0-3")).toHaveTextContent(/^111$/)
    expect(getByTestId("cell-1-6")).toHaveTextContent(/^3$/)
    expect(getByTestId("cell-2-2")).toHaveTextContent(/^15$/)
    expect(getByTestId("cell-4-4")).toHaveTextContent(/^7$/)
    expect(getByTestId("cell-5-0")).toHaveTextContent(/^4$/)
    expect(getByTestId("cell-6-3")).toHaveTextContent(/^13$/)
    expect(getByTestId("cell-6-6")).toHaveTextContent(/^11$/)
  })

  it("Allows shading cells", () => {
    const { getByTestId } = render(<SmallTapa />)
    const cell = getByTestId("cell-1-1")

    // Cell initially empty
    expect(cell).toHaveTextContent(/^$/)
    expect(cell.querySelector('[data-debug="shaded:UNKNOWN"]')).toBeTruthy()

    // Clicking a cell should change its value
    fireEvent.click(cell.querySelector('[data-debug="shaded:UNKNOWN"]'))
    expect(cell.querySelector('[data-debug="shaded:SHADED"]')).toBeTruthy()

    fireEvent.click(cell.querySelector('[data-debug="shaded:SHADED"]'))
    expect(cell.querySelector('[data-debug="shaded:UNSHADED"]')).toBeTruthy()
  })

  it("throws an error when given inconsistent props", () => {
    // Silent jsdom errors for this test (as there is an expected error)
    // See https://github.com/facebook/jest/pull/5267#issuecomment-356605468
    jest.spyOn(console, "error")
    console.error.mockImplementation(noop)

    expect(() =>
      render(<TapaSolver rows={3} columns={3} gridString="_,_,_,11,7" />),
    ).toThrow(
      new Error(
        "The given gridString is inconsistent with the number of rows and columns.",
      ),
    )

    // Restore console errors
    console.error.mockRestore()
  })

  it("can be updated to a new tapa", () => {
    const { container, rerender, getByTestId } = render(
      <TapaSolver rows={2} columns={2} gridString="_,3,_,_" />,
    )
    expect(container).toHaveTextContent(/^3$/)

    // Shade two cells
    fireEvent.click(
      getByTestId("cell-0-0").querySelector('[data-debug="shaded:UNKNOWN"]'),
    )
    fireEvent.click(
      getByTestId("cell-1-0").querySelector('[data-debug="shaded:UNKNOWN"]'),
    )

    // Updating to a new tapa should clear shaded cells
    rerender(
      <TapaSolver rows={3} columns={3} gridString="_,2,_,_,_,_,11,_,_" />,
    )
    expect(container).toHaveTextContent(/^211$/)
    expect(
      container.querySelectorAll('[data-debug="shaded:UNSHADED"]'),
    ).toHaveLength(0)
    expect(
      container.querySelectorAll('[data-debug="shaded:SHADED"]'),
    ).toHaveLength(0)
  })
})

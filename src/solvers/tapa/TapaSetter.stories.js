import React from "react"

import TapaSetter from "./TapaSetter"
import { action } from "@storybook/addon-actions"
import { number } from "@storybook/addon-knobs"

export default {
  title: "2. Setters/TapaSetter",
  component: TapaSetter,
}

export const Playground = () => {
  return (
    <TapaSetter
      rows={number("Number of rows", 8)}
      columns={number("Number of columns", 8)}
      onChange={action("Grid Changed")}
    />
  )
}

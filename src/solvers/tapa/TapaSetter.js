import React from "react"
import PropTypes from "prop-types"

import Grid from "components/Grid"
import CellBorders, { simpleGridProps } from "components/CellBorders"
import MultiClueCell from "components/MultiClueCell"
import useGridState from "hooks/useGridState"
import useClickOutside from "hooks/useClickOutside"
import SelectableCell from "components/SelectableCell"
import useSolverKeyboardEvents from "hooks/useSolverKeyboardEvents"
import { mapGridCoordinates, noop } from "utils"

const propTypes = {
  /**
   * Callback called each time the grid changes.
   *
   * It receives one parameter that can be fed to TapaSolver component.
   */
  onChange: PropTypes.func,
  /** Number of rows in the grid */
  rows: PropTypes.number.isRequired,
  /** Number of columns in the grid */
  columns: PropTypes.number.isRequired,
}

/**
 * Component allowing a user to enter a tapa grid.
 *
 * Select cells and use the digit keys to enter clues.
 */
export default function TapaSetter({ onChange = noop, rows, columns }) {
  // Values and selection.
  const { grid: clues, get: getClue, setAll: setClues } = useGridState(
    rows,
    columns,
    [],
  )
  const {
    get: isSelected,
    set: setSelected,
    reset: clearSelection,
  } = useGridState(rows, columns, false)
  const clickOutsideRef = useClickOutside(clearSelection)

  // When the value changes, send it to the parent
  React.useEffect(() => {
    onChange(
      clues
        .flat()
        .map(c => (c.length === 0 ? "_" : c.join("")))
        .join(","),
    )
  }, [onChange, clues])

  // Listen to keystrokes to enter clues because the ClueCell doesn't listen to events (it's made to be static)
  useSolverKeyboardEvents(({ value, nativeEvent }) => {
    setClues(
      mapGridCoordinates(rows, columns, (i, j) => {
        if (isSelected(i, j)) {
          return nativeEvent.code === "Backspace"
            ? []
            : [...getClue(i, j).slice(0, 3), value]
        } else {
          return getClue(i, j)
        }
      }),
    )
  })

  return (
    <div style={{ display: "inline-block" }} ref={clickOutsideRef}>
      <Grid rows={rows} columns={columns}>
        {(i, j) => (
          <SelectableCell
            selected={isSelected(i, j)}
            onSelect={() => {
              clearSelection()
              setSelected(i, j, true)
            }}
            onAddToSelection={() => setSelected(i, j, true)}
          >
            <CellBorders {...simpleGridProps(i, j)}>
              <MultiClueCell clues={getClue(i, j)} />
            </CellBorders>
          </SelectableCell>
        )}
      </Grid>
    </div>
  )
}
TapaSetter.propTypes = propTypes

const integerNotBetween = (min, max, v) =>
  typeof v !== "number" || v < min || v > max || !Number.isInteger(v)

/**
 * Use with propTypes to enforce a prop to be an integer between min and max inclusive.
 *
 * The prop can be undefined or null.
 */
export function optionalIntegerBetween(min, max) {
  return function(props, propName, componentName) {
    const prop = props[propName]
    if (
      prop !== null &&
      prop !== undefined &&
      integerNotBetween(min, max, prop)
    ) {
      return new Error(
        `Invalid prop ${propName} (value: ${prop}) supplied to ${componentName}: it must be an integer between ${min} and ${max} inclusive.`,
      )
    }
  }
}

/**
 * Use with propTypes to enforce a prop to be an integer between min and max inclusive.
 *
 * The prop must be present (not undefined nor null)
 */
export function integerBetween(min, max) {
  return function(props, propName, componentName) {
    const prop = props[propName]
    if (integerNotBetween(min, max, prop)) {
      return new Error(
        `Invalid prop ${propName} (value: ${prop}) supplied to ${componentName}: it must be an integer between ${min} and ${max} inclusive.`,
      )
    }
  }
}

import React from "react"

import useUniqueId from "./useUniqueId"

// Store all callbacks to call when events are detected.
const subscribed = {}

// Add a single global event listener. When we detect a keypress, we send the event to a list of subscribers.
// This avoids creating a different event listener for each element that listens to the events.
document.addEventListener("mousedown", event => {
  Object.values(subscribed).forEach(callback => {
    callback(event)
  })
})

/**
 * Listen to clicks that are outside a particular element.
 *
 * The element must be set using the returned ref.
 *
 * @param callback the function to call when a click outside the element is detected
 */
export default function useClickOutside(callback) {
  const key = useUniqueId()
  const element = React.useRef(null)
  React.useEffect(() => {
    const cb = event => {
      if (
        element.current !== null &&
        event.target.isConnected &&
        !element.current.contains(event.target)
      ) {
        callback(event)
      }
    }
    subscribed[key] = cb
    return () => delete subscribed[key]
  }, [key, element, callback])

  return e => (element.current = e)
}

// Generally we prefer testing the hooks alongside the components that use them.
// This hook is thoroughly tested in the Solvers test cases (for example in ClassicSudokuSolver.spec.js)
// However, this hook has an error case that should never happen from a component (it would be a bug)
// So we test this error case here.

import { renderHook } from "@testing-library/react-hooks"
import useGridState from "./useGridState"

describe("useGridState", () => {
  it("should throw an error when updating to an inconsistent value", () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const hookUsage = () => useGridState(2, 2, "a")
    const hookResult = renderHook(hookUsage)

    expect(hookResult.result.current.grid).toEqual([["a", "a"], ["a", "a"]])

    expect(() => hookResult.result.current.setAll([["a", "b"], ["c"]])).toThrow(
      new Error(
        "useGridState state must be a two-dimensional array with size matching given i and j parameters",
      ),
    )
  })
})

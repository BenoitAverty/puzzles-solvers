import * as React from "react"

let nextId = 0
export default function useUniqueId() {
  const keyRef = React.useRef(nextId++)
  return keyRef.current
}

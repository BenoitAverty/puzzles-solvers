import React from "react"
import useUniqueId from "./useUniqueId"

const keyCodeMapping = {
  Digit1: 1,
  Digit2: 2,
  Digit3: 3,
  Digit4: 4,
  Digit5: 5,
  Digit6: 6,
  Digit7: 7,
  Digit8: 8,
  Digit9: 9,
  Backspace: null,
}

// Store all callbacks to call when events are detected.
const subscribed = {}
// Add a single global event listener. When we detect a keypress, we send the event to a list of subscribers.
// This avoids creating a different event listener for each element that listens to the events.
document.addEventListener("keydown", event => {
  Object.values(subscribed).forEach(callback => {
    if (Object.prototype.hasOwnProperty.call(keyCodeMapping, event.code)) {
      event.preventDefault()
      callback({ value: keyCodeMapping[event.code], nativeEvent: event })
    }
  })
})

/**
 * Listen to keyboard events that are relevant to the solver components.
 * This includes :
 * <ul>
 *   <li>Numbers (keypad and number row)</li>
 *   <li>Arrows</li>
 *   <li>Esc Key</li>
 * </ul>
 * @param callback the function to call when a relevant keypress is detected
 */
export default function useSolverKeyboardEvents(callback) {
  const key = useUniqueId()
  React.useEffect(() => {
    subscribed[key] = callback
    return () => delete subscribed[key]
  }, [key, callback])
}

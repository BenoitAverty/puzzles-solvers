import React from "react"

import { mapGridCoordinates, range } from "utils"

function checkState(i, j, state) {
  if (
    !Array.isArray(state) ||
    state.length !== i ||
    state.some(row => !Array.isArray(row)) ||
    state.some(row => row.length !== j)
  ) {
    throw new Error(
      "useGridState state must be a two-dimensional array with size matching given i and j parameters",
    )
  }
}

export default function useGridState(rows, columns, initialFillValue) {
  const initialState = React.useMemo(
    () => range(0, rows, range(0, columns, initialFillValue)),
    [rows, columns, initialFillValue],
  )
  const [grid, setGrid] = React.useState(initialState)

  // Update the state if the grid is resized
  React.useEffect(() => {
    const newState = mapGridCoordinates(rows, columns, (i, j) => {
      if (grid && grid[i] && grid[i][j]) {
        return grid[i][j]
      } else return initialFillValue
    })
    setGrid(newState)
  }, [rows, columns]) // eslint-disable-line react-hooks/exhaustive-deps

  return {
    grid,
    // Returns the value of a cell
    get: (i, j) => (grid && grid[i] && grid[i][j]) || initialFillValue,
    // Sets the value of a cell
    set: (i, j, value) => {
      const updateWithNewValue = currentGrid => [
        ...currentGrid.slice(0, i),
        [...currentGrid[i].slice(0, j), value, ...currentGrid[i].slice(j + 1)],
        ...currentGrid.slice(i + 1),
      ]
      setGrid(updateWithNewValue)
    },
    // Set a complete grid
    setAll: newGrid => {
      checkState(rows, columns, newGrid)
      setGrid(newGrid)
    },
    // Resets the grid to the initial fill value
    reset: React.useCallback(() => setGrid(initialState), [
      setGrid,
      initialState,
    ]),
  }
}

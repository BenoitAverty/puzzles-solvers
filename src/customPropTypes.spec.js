import { integerBetween, optionalIntegerBetween } from "./customPropTypes"

describe("Custom Prop Types", () => {
  describe("optionalIntegerBetween", () => {
    test("prop lower than minimal value", () => {
      const validator = optionalIntegerBetween(1, 9)

      expect(validator({ test: 0 }, "test", "AComponent")).toEqual(
        new Error(
          "Invalid prop test (value: 0) supplied to AComponent: it must be an integer between 1 and 9 inclusive.",
        ),
      )
    })
    test("prop is not an integer", () => {
      const validator = optionalIntegerBetween(1, 9)

      expect(validator({ test: 1.5 }, "test", "AComponent")).toEqual(
        new Error(
          "Invalid prop test (value: 1.5) supplied to AComponent: it must be an integer between 1 and 9 inclusive.",
        ),
      )
    })
    test("prop is undefined", () => {
      const validator = optionalIntegerBetween(1, 9)

      expect(validator({ other: 8 }, "test", "AComponent")).toBeUndefined()
    })
  })

  describe("integerBetween", () => {
    test("prop lower than minimal value", () => {
      const validator = integerBetween(1, 9)

      expect(validator({ test: 0 }, "test", "AComponent")).toEqual(
        new Error(
          "Invalid prop test (value: 0) supplied to AComponent: it must be an integer between 1 and 9 inclusive.",
        ),
      )
    })
    test("prop is not an integer", () => {
      const validator = integerBetween(1, 9)

      expect(validator({ test: 1.5 }, "test", "AComponent")).toEqual(
        new Error(
          "Invalid prop test (value: 1.5) supplied to AComponent: it must be an integer between 1 and 9 inclusive.",
        ),
      )
    })
    test("prop is undefined", () => {
      const validator = integerBetween(1, 9)

      expect(validator({ other: 8 }, "test", "AComponent")).toEqual(
        new Error(
          "Invalid prop test (value: undefined) supplied to AComponent: it must be an integer between 1 and 9 inclusive.",
        ),
      )
    })
  })
})
